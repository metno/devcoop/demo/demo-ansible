# -*- coding: utf-8 -*-
# Name and plot string of Quick-menu
# '#' marks comments
#------------------------------------------------
#-- Name of quick-menu, given in the dialog
# "name"
#------------------------------------------------
#-- Variable definitions
# [XX=a,b,c,d,..]
#------------------------------------------------
#-- Plots
# '>Plot name' is separator between the different plots
#------------------------------------------------

# quickmenu name
"Model fields SA4 INAM"

# variables

#
>MSLP, 6h precip
FIELD model=SA4_INAM plot=MSLP colour=blue plottype=contour linetype=solid linewidth=1 base=0 frame=1 line.interval=1 extreme.type=None extreme.size=1 extreme.radius=1 palettecolours=off table=1 repeat=0 value.label=1 line.smooth=4 field.smooth=10 label.size=1 grid.lines=0 grid.lines.max=0 undef.masking=0 undef.colour=255:255:255:255 undef.linewidth=1 undef.linetype=solid grid.value=0 colour_2=off dim=1 text=""Mean units=hectopascal Sea Level Pressure""
FIELD model=SA4_INAM plot=PRECIP.6T colour=red plottype=contour linetype=empty linewidth=1 base=0 frame=1 line.values=0.5,1,2,4,6,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100 extreme.type=None extreme.size=1 extreme.radius=1 palettecolours=precipitation table=1 repeat=0 value.label=0 line.smooth=0 field.smooth=0 label.size=1 grid.lines=0 grid.lines.max=0 undef.masking=0 undef.colour=255:255:255:255 undef.linewidth=1 undef.linetype=solid grid.value=0 colour_2=off dim=1
AREA name=Africa
MAP map=Gshhs-Auto contour=on cont.colour=black cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=on land.colour=lightgray land.zorder=0
MAP map=Africa contour=on cont.colour=black cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=off
MAP map=mozambique_level1_regions contour=on cont.colour=darkgrey cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=off
MAP backcolour=seablue2 lon=on lon.colour=darkgray lon.linewidth=1 lon.linetype=solid lon.density=10 lon.zorder=1 lon.showvalue=off lon.value_pos= lon.fontsize=10 lat=on lat.colour=darkgray lat.linewidth=1 lat.linetype=solid lat.density=10 lat.zorder=1 lat.showvalue=off lat.value_pos= lat.fontsize=10 frame=off
LABEL data font=BITMAPFONT fontsize=12
LABEL text="$day $date $auto UTC" tcolour=red bcolour=black fcolour=white:200 polystyle=both halign=left valign=top font=BITMAPFONT fontsize=12
LABEL anno=<table,fcolour=white:150> halign=right valign=top polystyle=none margin=0 fontsize=12
LABEL anno=<arrow> halign=right valign=bottom fcolour=220:220:220:160 fontsize=12
#
>Wind 10m
FIELD model=SA4_INAM plot=MSLP colour=blue plottype=contour linetype=solid linewidth=1 base=0 frame=1 line.interval=1 extreme.type=None extreme.size=1 extreme.radius=1 palettecolours=off table=1 repeat=0 value.label=1 line.smooth=4 field.smooth=10 label.size=1 grid.lines=0 grid.lines.max=0 undef.masking=0 undef.colour=255:255:255:255 undef.linewidth=1 undef.linetype=solid grid.value=0 colour_2=off dim=1 text=""Mean units=hectopascal Sea Level Pressure""
FIELD model=SA4_INAM plot=WIND.10M colour=red plottype=wind linetype=solid linewidth=1 base=0 frame=1 vector.unit=10 line.smooth=0 field.smooth=0 label.size=1 grid.lines=0 grid.lines.max=0 undef.masking=0 undef.colour=255:255:255:255 undef.linewidth=1 undef.linetype=solid grid.value=0 colour_2=off dim=2
FIELD model=SA4_INAM plot=FF.10M.KNOTS colour=0:0:0:255 plottype=contour linetype=solid linewidth=1 base=0 frame=1 line.values=15.5,20.9,27,33.4,40.4,47.6,55.4,63.5 extreme.type=None extreme.size=1 extreme.radius=1 palettecolours=wms.ff10m.palett table=1 repeat=0 value.label=1 line.smooth=0 field.smooth=0 label.size=1 grid.lines=0 grid.lines.max=0 undef.masking=0 undef.colour=255:255:255:255 undef.linewidth=1 undef.linetype=solid grid.value=0 colour_2=off dim=1 text=""Windspeed in beaufort/kt""
AREA name=Africa
MAP map=Gshhs-Auto contour=on cont.colour=black cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=on land.colour=lightgray land.zorder=0
MAP map=Africa contour=on cont.colour=black cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=off
MAP map=mozambique_level1_regions contour=on cont.colour=darkgrey cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=off
MAP backcolour=seablue2 lon=on lon.colour=darkgray lon.linewidth=1 lon.linetype=solid lon.density=10 lon.zorder=1 lon.showvalue=off lon.value_pos= lon.fontsize=10 lat=on lat.colour=darkgray lat.linewidth=1 lat.linetype=solid lat.density=10 lat.zorder=1 lat.showvalue=off lat.value_pos= lat.fontsize=10 frame=off
LABEL data font=BITMAPFONT fontsize=12
LABEL text="$day $date $auto UTC" tcolour=red bcolour=black fcolour=white:200 polystyle=both halign=left valign=top font=BITMAPFONT fontsize=12
LABEL anno=<table,fcolour=white:150> halign=right valign=top polystyle=none margin=0 fontsize=12
LABEL anno=<arrow> halign=right valign=bottom fcolour=220:220:220:160 fontsize=12
#
>Temperature
FIELD model=SA4_INAM plot=T.2M colour=darkred plottype=contour linetype=empty linewidth=1 base=10 frame=1 line.interval=2 extreme.type=Maxvalue extreme.size=0.4 extreme.radius=1 palettecolours=temperature_warm,temperature_cold table=1 repeat=0 value.label=0 line.smooth=0 field.smooth=0 label.size=1 grid.lines=0 grid.lines.max=0 undef.masking=0 undef.colour=255:255:255:255 undef.linewidth=1 undef.linetype=solid grid.value=0 colour_2=off dim=1 text=""2m Temperature"" units=celsius text=""2m Temperature"" units=celsius
AREA name=Africa
MAP map=Gshhs-Auto contour=on cont.colour=black cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=on land.colour=lightgray land.zorder=0
MAP map=Africa contour=on cont.colour=black cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=off
MAP map=mozambique_level1_regions contour=on cont.colour=darkgrey cont.linewidth=1 cont.linetype=solid cont.zorder=1 land=off
MAP backcolour=seablue2 lon=on lon.colour=darkgray lon.linewidth=1 lon.linetype=solid lon.density=10 lon.zorder=1 lon.showvalue=off lon.value_pos= lon.fontsize=10 lat=on lat.colour=darkgray lat.linewidth=1 lat.linetype=solid lat.density=10 lat.zorder=1 lat.showvalue=off lat.value_pos= lat.fontsize=10 frame=off
LABEL data font=BITMAPFONT fontsize=12
LABEL text="$day $date $auto UTC" tcolour=red bcolour=black fcolour=white:200 polystyle=both halign=left valign=top font=BITMAPFONT fontsize=12
LABEL anno=<table,fcolour=white:150> halign=right valign=top polystyle=none margin=0 fontsize=12
LABEL anno=<arrow> halign=right valign=bottom fcolour=220:220:220:160 fontsize=12
