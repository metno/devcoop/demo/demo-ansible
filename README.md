# ansible configuration for demo MET workstation

## running the configuration scripts

Create a file `$HOME/ansible-vault-pass` containing the ansible vault
password.

1. update the scripts from the git repository

        git pull

2. activate the ansible virtual environment

        . "$HOME/venv/ansible/bin/activate"

3. if `requirements.yml` has changed, update the external roles

        ansible-galaxy install -r requirements.yml -f

4. run the ansible confiuration scripts

        export ANSIBLE_VAULT_PASSWORD_FILE="$HOME/ansible-vault-pass"
        ansible-playbook -i hosts_demo site.yml


## installing ansible

Run these commands:

    sudo apt-get install -y python3-virtualenv python3-setuptools python3-pip python3-dev python-setuptools virtualenv
    mkdir "$HOME/venv"
    virtualenv -p python3 "$HOME/venv/ansible"
    . "$HOME/venv/ansible/bin/activate"
    pip install -U pip
    pip install -U ansible
